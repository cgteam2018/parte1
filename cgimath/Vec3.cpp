#include "Vec3.hpp"
#include <math.h>
#include <iostream>

Vec3::Vec3() {
    x = 0;
    y = 0;
    z = 0;
}

Vec3::Vec3(float i, float j, float k) {
    x = i;
    y = j;
    z = k;
}

/*Metodos de acesso*/
float Vec3::operator [] (int position) const {
    switch(position) {
        case 0 : return x;
        case 1 : return y;
        case 2 : return z;
    }
}

float& Vec3::operator [] (int position) {
    switch(position) {
        case 0 : return this->x;
        case 1 : return this->y;
        case 2 : return this->z;
    }
}
/*Fim dos m�todos de acesso*/

/*Opera��es com escalar*/

const Vec3 Vec3::operator * (float scalar) const {
    float xr = x*scalar;
    float yr = y*scalar;
    float zr = z*scalar;

    Vec3 result(xr, yr, zr);
    return result;
}

const Vec3 Vec3::operator / (float scalar) const {
    float xr = x/scalar;
    float yr = y/scalar;
    float zr = z/scalar;

    Vec3 result(xr, yr, zr);
    return result;
}

const void Vec3::operator *= (float scalar) {
    x = x*scalar;
    y = y*scalar;
    z = z*scalar;
}

const void Vec3::operator /= (float scalar) {
    x = x/scalar;
    y = y/scalar;
    z = z/scalar;
}
/*Fim das opera��es com escalar*/

/*Opera��es com vetor*/
const Vec3 Vec3::operator + (const Vec3& vec) const {
    int xr = x + vec[0];
    int yr = y + vec[1];
    int zr = z + vec[2];

    Vec3 result(xr, yr, zr);
    return result;
}

const Vec3 Vec3::operator - (const Vec3& vec) const {
    int xr = x - vec[0];
    int yr = y - vec[1];
    int zr = z - vec[2];

    Vec3 result(xr, yr, zr);
    return result;
}

const void Vec3::operator += (const Vec3& vec) {
    x = x + vec[0];
    y = y + vec[1];
    z = z + vec[2];
}

const void Vec3::operator -= (const Vec3& vec) {
    x = x - vec[0];
    y = y - vec[1];
    z = z - vec[2];
}

const bool Vec3::operator == (const Vec3& vec) const {
    if(floor(vec[0])==floor(x) && floor(vec[1])==floor(y) && floor(vec[2])==floor(z)) {
        return true;
    } else {
        return false;
    }
}

const bool Vec3::operator != (const Vec3& vec) const {
    if(floor(vec[0])!=floor(x) || floor(vec[1])!=floor(y) || floor(vec[2])!=floor(z)) {
        return true;
     } else {
        return false;
    }
}
/*Fim das opera��es com vetor*/

/* vetores apontando para cada dire��o*/
const Vec3 Vec3::UP() {
    Vec3 result(0,1,0);
    return result;
}

const Vec3 Vec3::DOWN() {
    Vec3 result(0,-1,0);
    return result;
}

const Vec3 Vec3::LEFT() {
    Vec3 result(-1,0,0);
    return result;
}

const Vec3 Vec3::RIGHT() {
    Vec3 result(1,0,0);
    return result;
}

const Vec3 Vec3::BACK() {
    Vec3 result(0,0,-1);
    return result;
}

const Vec3 Vec3::FOWARD() {
    Vec3 result(0,0,1);
    return result;
}

/* vetores (1, 1, 1) e (0, 0, 0)*/
const Vec3 Vec3::ONE() {
    Vec3 vOne(1, 1, 1);
    return vOne;
}

const Vec3 Vec3::ZERO() {
    Vec3 vZero(0, 0, 0);
    return vZero;
}

/* operacoes entre vetores*/
// produto escalar
float Vec3::dot_product(const Vec3& vecA,const Vec3& vecB) {
    return vecA[0]*vecB[0] + vecA[1]*vecB[1] + vecA[2]*vecB[2];
}

// produto vetorial
const Vec3 Vec3::cross_product(const Vec3& vecA,const Vec3& vecB) {
    float xr = vecA[1]*vecB[2] - vecA[2]*vecB[1];
    float yr = vecA[2]*vecB[0] - vecA[0]*vecB[2];
    float zr = vecA[0]*vecB[1] - vecA[1]*vecB[0];

    Vec3 result(xr, yr, zr);
    return result;
}

// comprimento
float Vec3::magnitude(const Vec3& vec) {
    float sum = pow(vec[0], 2) + pow(vec[1], 2) + pow(vec[2], 2);
    float mag = sqrt(sum);

    return mag;
}

// Vetor Normalizado
const Vec3 Vec3::normalized(const Vec3& vec) {
    float mag = magnitude(vec);

    float xr = vec[0] / mag;
    float yr = vec[1] / mag;
    float zr = vec[2] / mag;

    Vec3 result(xr, yr, zr);
    return result;
}

//angulo entre dois vetores
const float Vec3::angle(const Vec3& vecA, const Vec3& vecB) {
    float dot_prod = dot_product(vecA, vecB);
    float mag = magnitude(vecA) * magnitude(vecB);
    float result = acos(dot_prod/mag);
    result = result * 180/ M_PI;
    return result;
}

// transformações de coordenadas cartesianas para esféricas e vice-versa
const Vec3 Vec3::spherical(const Vec3& vec) {
    float x = vec[0];
    float y = vec[1];
    float z = vec[2];

    float r = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
    float theta = atan(y / x) * 180 / M_PI;
    float phi = atan(sqrt(pow(x, 2) + pow(y, 2)) / z) * 180 / M_PI;

    Vec3 svec (r, theta , phi);
    return svec;
}

const Vec3 Vec3::cartesian(const Vec3& vec) {
    float r = vec[0];
    float theta = vec[1] * M_PI / 180;
    float phi = vec[2] * M_PI / 180;

    float x = r * sin(phi) * cos(theta);
    float y = r * sin(phi) * sin(theta);
    float z = r * cos(phi);

    Vec3 cvec (x, y, z);
    return cvec;
}

void Vec3::to_string() {
    std::cout << "(" << x << ", " << y << ", " << z << ")" << "\n";
}