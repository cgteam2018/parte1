#include "Vec4.h"
#include "Vec3.h"

class Mat4 {
    private:
        Vec4 c0, c1, c2, c3; // vetores coluna
    
    public:
        // construtores
        Mat4();     // inicia a matriz identidade

        Mat4(const Vec4& vecA, const Vec4& vecB, const Vec4& vecC, const Vec4& vecD);  // c0 = A, c1 = B, c2 = C, c3 = D

        Mat4(float a00, float a01, float a02, float a03,
             float a10, float a11, float a12, float a13,
             float a20, float a21, float a22, float a23,   // c0 = (a00, a10, a20, a30), c1 = (a01, a11, a21, a31),
             float a30, float a31, float a32, float a33);  // c2 = (a02, a12, a22, a32), c3 = (a03, a13, a23, a33)

        Mat4(const Vec3& vecA, const Vec3& vecB, const Vec3& vecC);  // c0 = (A[0], A[1], A[2], 0), c1 = (B[0], B[1], B[2], 0)
                                                                     // c2 = (C[0], C[1], C[2], 0), c2 = (0, 0, 0, 1)

        // sobrecarga de operadores

        // acesso de COLUNA via índice (c0 = mat[0], c1=mat[1], c2=mat[2])
        Vec4 operator [] (int col) const;
		Vec4& operator [] (int col);

        // acesso a membros via (coluna, linha)
        float  operator () (int col, int row) const;
        float& operator () (int col, int row);

        // matriz com escalar
        const Mat4 operator * (float scalar) const;
        const Mat4 operator / (float scalar) const;
        const void operator *= (float scalar);
        const void operator /= (float scalar);

        // matriz com matriz
        const Mat4 operator + (const Mat4& mat) const;
        const Mat4 operator - (const Mat4& mat) const;
        const Mat4 operator * (const Mat4& mat) const;
        const void operator += (const Mat4& mat);
        const void operator -= (const Mat4& mat);
        const void operator *= (const Mat4& mat);
        const bool operator == (const Mat4& vec) const;
        const bool operator != (const Mat4& vec) const;

        // operações com matrizes
        static const Mat4 transpose(const Mat4& mat);    // transposta
        static const float det(const Mat4& mat);     // determinante
        static const Mat4 cof(const Mat4& mat);      // mat. de cofatores
        static const Mat4 inv(const Mat4& mat);  // inversa

        // // daqui pra baixo entendo muita coisa, creio que não precise implementar agora

        // // não faço ideia do que seja
        // static const Mat4 lookAt(const Vec4& vecA, const Vec4& vecB); // eye,ups

		// // Matriz de projeção ortogonal, definição do frustum: left, right, top,
		// // bottom, near, far
		// static const Mat4 ortho(float left, float right, float top, float bottom, float near, float far);

		// // ortogonal simplificada; aspect, scale, near, far
		// static const Mat4 ortho(float aspect, float scale, float near, float far);

		// // Projeção perspectiva: aspect, FOV, near, far
		// static const Mat4 proj(float aspect, float FOV, float near, float far);

		// // perspectiva infinita: aspect, FOV, near
		// static const Mat4 proj(float aspect, float FOV, float near);

        // representação visual
        void to_string();
};