#include "Vec4.hpp"
#include <iostream>
#include <math.h>

Vec4::Vec4() {
    x = 0.;
    y = 0.;
    z = 0.;
    w = 0.;
}

Vec4::Vec4(float i, float j, float k, float l) {
    x = i;
    y = j;
    z = k;
    w = l;
}

/*Metodos de acesso*/
float Vec4::operator [] (int position) const {
    switch(position) {
        case 0 : return x;
        case 1 : return y;
        case 2 : return z;
        case 3 : return w;
    }
}

float& Vec4::operator [] (int position) {
    switch(position) {
        case 0 : return this->x;
        case 1 : return this->y;
        case 2 : return this->z;
        case 3 : return this->w;
    }
}
/*Fim dos metodos de acesso*/

/*Operacoes com escalar*/

const Vec4 Vec4::operator * (float scalar) const{
    float xr = x*scalar;
    float yr = y*scalar;
    float zr = z*scalar;
	float wr = w*scalar;

    Vec4 result(xr, yr, zr, wr);
    return result;
}

const Vec4 Vec4::operator / (float scalar) const{
    float xr = x/scalar;
    float yr = y/scalar;
    float zr = z/scalar;
	float wr = w/scalar;

    Vec4 result(xr, yr, zr, wr);
    return result;
}
const void Vec4::operator *= (float scalar){
    x=x*scalar;
    y=y*scalar;
    z=z*scalar;
	w=w*scalar;
}
const void Vec4::operator /= (float scalar){
    x=x/scalar;
    y=y/scalar;
    z=z/scalar;
	w=w/scalar;
}
/*Fim das operacoes com escalar*/

/*Operacoes com vetor*/
const Vec4 Vec4::operator + (const Vec4& vec) const{
    float xr = x + vec[0];
    float yr = y + vec[1];
    float zr = z + vec[2];
    float wr = w + vec[3];

    Vec4 result(xr, yr, zr, wr);
    return result;
}
const Vec4 Vec4::operator - (const Vec4& vec) const{
    float xr = x - vec[0];
    float yr = y - vec[1];
    float zr = z - vec[2];
    float wr = w - vec[3];

    Vec4 result(xr, yr, zr, wr);
    return result;
}
const bool Vec4::operator == (const Vec4& vec) const{
    if(floor(vec[0])==floor(x) && floor(vec[1])==floor(y) && floor(vec[2])==floor(z) && floor(vec[3]) == floor(w)){
        return true;
    }else{
        return false;
    }

}
const bool Vec4::operator != (const Vec4& vec) const{
    if(floor(vec[0])!=floor(x) || floor(vec[1])!=floor(y) || floor(vec[2])!=floor(z) || floor(vec[3])!=floor(w)){
        return true;
    }else{
        return false;
    }
}
const void Vec4::operator += (const Vec4& vec){
    x = x + vec[0];
    y = y + vec[1];
    z = z + vec[2];
    w = w + vec[3];
}
const void Vec4::operator -= (const Vec4& vec){
    x = x - vec[0];
    y = y - vec[1];
    z = z - vec[2];
    w = w - vec[3];
}
/*Fim das operacoes com vetor*/

/* vetores apontando para cada direcao*/
const Vec4 Vec4::UP(){
    Vec4 result(0,1,0,0);
    return result;
}
const Vec4 Vec4::DOWN(){
    Vec4 result(0,-1,0,0);
    return result;
}
const Vec4 Vec4::LEFT(){
    Vec4 result(-1,0,0,0);
    return result;
}
const Vec4 Vec4::RIGHT(){
    Vec4 result(1,0,0,0);
    return result;
}
const Vec4 Vec4::BACK(){
    Vec4 result(0,0,-1,0);
    return result;
}
const Vec4 Vec4::FOWARD(){
    Vec4 result(0,0,1,0);
    return result;
}

/* vetores (1, 1, 1) e (0, 0, 0)*/
const Vec4 Vec4::ONE(){
    static Vec4 vOne(1, 1, 1, 1);
    return vOne;
}
const Vec4 Vec4::ZERO(){
    static Vec4 vZero(0, 0, 0, 0);
    return vZero;
}

/* operacoes entre vetores*/
// produto escalar
float Vec4::dot_product(const Vec4& vecA,const Vec4& vecB) {
    return vecA[0]*vecB[0] + vecA[1]*vecB[1] + vecA[2]*vecB[2] + vecA[3]*vecB[3];
}

// comprimento
float Vec4::magnitude(const Vec4& vec) {
    float sum = pow(vec[0], 2) + pow(vec[1], 2) + pow(vec[2], 2) + pow(vec[3], 2);
    float mag = sqrt(sum);

    return mag;
}

// Vetor Normalizado
const Vec4 Vec4::normalized(const Vec4& vec) {
    float mag = magnitude(vec);

    float xr = vec[0] / mag;
    float yr = vec[1] / mag;
    float zr = vec[2] / mag;
    float wr = vec[3] / mag;

    Vec4 result(xr, yr, zr, wr);
    return result;
}

void Vec4::to_string() {
    std::cout << "(" << x << ", " << y << ", " << z << ", " << w << ")" << "\n";
}
