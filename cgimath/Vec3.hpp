// #include Mat3.h
#ifndef VEC3_H
#define VEC3_h

class Vec3 {
    private:
        float x, y, z;  // coordenadas do vetor
    
    public:
        // construtores
        Vec3(); // inicia com (0, 0, 0)
        Vec3(float i, float j, float k);    // inicia x = i, y = j, z = k

        // sobrecarga de operadores

        // acesso via índices
        float operator [] (int i) const;
        float& operator [] (int i);

        // vetor com escalar
        const Vec3 operator * (float scalar) const;
        const Vec3 operator / (float scalar) const;
        const void operator *= (float scalar);
        const void operator /= (float scalar);

        //vetor com vetor
        const Vec3 operator + (const Vec3& vec) const;
        const Vec3 operator - (const Vec3& vec) const;
        const bool operator == (const Vec3& vec) const;
        const bool operator != (const Vec3& vec) const;
        const void operator += (const Vec3& vec);
        const void operator -= (const Vec3& vec);

        // matriz com vetor
        // const void operator * (const Mat3& mat); // supostamente aplica uma matriz ao vetor, não sei se é pra implementar

        // vetores apontando para cada direção
        static const Vec3 UP();
        static const Vec3 DOWN();
        static const Vec3 LEFT();
        static const Vec3 RIGHT();
        static const Vec3 BACK();
        static const Vec3 FOWARD();

        // vetores (1, 1, 1) e (0, 0, 0)
        static const Vec3 ONE();
        static const Vec3 ZERO();

        // operacoes entre vetores
        static float dot_product(const Vec3& vecA,const Vec3& vecB);      // produto escalar
        static const Vec3 cross_product(const Vec3& vecA,const Vec3& vecB);   // produto vetorial
        static float magnitude(const Vec3& vec);     // tamanho
		static const Vec3 normalized(const Vec3& vec);   // norma
        static const float angle(const Vec3& vecA, const Vec3& vecB);

        // transformações de coordenadas cartesianas para esféricas e vice-versa
        static const Vec3 spherical(const Vec3& vec);
		static const Vec3 cartesian(const Vec3& vec);

        // representação visual
        void to_string();
};

#endif