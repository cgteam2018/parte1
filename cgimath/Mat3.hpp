#include "Vec3.hpp"

class Mat3 {
    private:
        Vec3 c0, c1, c2; // vetores coluna
    
    public:
        // construtores
        Mat3();     // inicia a matriz identidade
        Mat3(const Vec3& vecA, const Vec3& vecB, const Vec3& vecC);  // c0 = A, c1 = B, c2 = C
        Mat3(float a00, float a01, float a02, 
             float a10, float a11, float a12,
             float a20, float a21, float a22);  // c0 = (a00, a10, a20), c1 = (a01, a11, a21), c2 = (a02, a12, a22)

        // sobrecarga de operadores

        // acesso de COLUNA via índice (c0 = mat[0], c1=mat[1], c2=mat[2])
        Vec3 operator [] (int col) const;
		Vec3& operator [] (int col);

        // acesso a membros via (coluna, linha)
        float  operator () (int col, int row) const;
        float& operator () (int col, int row);

        // matriz com escalar
        const Mat3 operator * (float scalar) const;
        const Mat3 operator / (float scalar) const;
        const void operator *= (float scalar);
        const void operator /= (float scalar);

        // matriz com matriz
        const Mat3 operator + (const Mat3& mat) const;
        const Mat3 operator - (const Mat3& mat) const;
        const Mat3 operator * (const Mat3& mat) const;
        const void operator += (const Mat3& mat);
        const void operator -= (const Mat3& mat);
        const void operator *= (const Mat3& mat);
        const bool operator == (const Mat3& mat) const;
        const bool operator != (const Mat3& mat) const;

        // operações com matrizes
        static const Mat3 transpose(const Mat3& mat);    // transposta
        static const float det(const Mat3& mat);     // determinante
        //static const Mat3 cof(const Mat3& mat);      // mat. de cofatores // deixa pra depois
        static const Mat3 inv(const Mat3& mat);  // inversa

        // não faço ideia do que seja
        // static const Mat3 lookAt(const Vec3& vecA, const Vec3& vecB); // eye,ups

        // representação visual
        void to_string();
};