#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "Mat3.hpp"
#include <iostream>

TEST_CASE("Mat3()", "") {
    Mat3 mat;

    REQUIRE( mat(0,0) == 1.f );
    REQUIRE( mat(0,1) == 0.f );
    REQUIRE( mat(0,2) == 0.f );
    REQUIRE( mat(1,0) == 0.f );
    REQUIRE( mat(1,1) == 1.f );
    REQUIRE( mat(1,2) == 0.f );
    REQUIRE( mat(2,0) == 0.f );
    REQUIRE( mat(2,1) == 0.f );
    REQUIRE( mat(2,2) == 1.f );
}

TEST_CASE("Mat3(Vec3, Vec3, Vec3)", "") {
    Vec3 vecA (1, 0, 0);
    Vec3 vecB(0, 1, 0);
    Vec3 vecC (0, 0, 1);
    Mat3 mat (vecA, vecB, vecC);

    REQUIRE( mat(0,0) == 1.f );
    REQUIRE( mat(0,1) == 0.f );
    REQUIRE( mat(0,2) == 0.f );
    REQUIRE( mat(1,0) == 0.f );
    REQUIRE( mat(1,1) == 1.f );
    REQUIRE( mat(1,2) == 0.f );
    REQUIRE( mat(2,0) == 0.f );
    REQUIRE( mat(2,1) == 0.f );
    REQUIRE( mat(2,2) == 1.f );
}

TEST_CASE("Mat3(float, float, ...)", "") {
    Mat3 mat (1, 0, 0, 0, 1, 0, 0, 0, 1);

    REQUIRE( mat(0,0) == 1.f );
    REQUIRE( mat(0,1) == 0.f );
    REQUIRE( mat(0,2) == 0.f );
    REQUIRE( mat(1,0) == 0.f );
    REQUIRE( mat(1,1) == 1.f );
    REQUIRE( mat(1,2) == 0.f );
    REQUIRE( mat(2,0) == 0.f );
    REQUIRE( mat(2,1) == 0.f );
    REQUIRE( mat(2,2) == 1.f );
}

TEST_CASE("operator []", "") {
    Mat3 mat;
    Vec3 vecB = mat[0];
    Vec3 vecC (1, 1, 1);
    mat[0] = vecC;

    REQUIRE( mat(0,0) == 1.f );
    REQUIRE( mat(0,1) == 1.f );
    REQUIRE( mat(0,2) == 1.f );
    REQUIRE( mat(1,0) == 0.f );
    REQUIRE( mat(1,1) == 1.f );
    REQUIRE( mat(1,2) == 0.f );
    REQUIRE( mat(2,0) == 0.f );
    REQUIRE( mat(2,1) == 0.f );
    REQUIRE( mat(2,2) == 1.f );

    REQUIRE( vecB[0] == 1.f );
    REQUIRE( vecB[1] == 0.f );
    REQUIRE( vecB[2] == 0.f );
}

TEST_CASE("operator ()", "") {
    Mat3 mat;
    mat(0,2) = 5.f;

    REQUIRE( mat(0,0) == 1.f );
    REQUIRE( mat(0,1) == 0.f );
    REQUIRE( mat(0,2) == 5.f );
    REQUIRE( mat(1,0) == 0.f );
    REQUIRE( mat(1,1) == 1.f );
    REQUIRE( mat(1,2) == 0.f );
    REQUIRE( mat(2,0) == 0.f );
    REQUIRE( mat(2,1) == 0.f );
    REQUIRE( mat(2,2) == 1.f );
}

TEST_CASE("operator * scalar", "") {
    Mat3 mat;

    mat = mat * 3;

    REQUIRE( mat(0,0) == 3.f );
    REQUIRE( mat(0,1) == 0.f );
    REQUIRE( mat(0,2) == 0.f );
    REQUIRE( mat(1,0) == 0.f );
    REQUIRE( mat(1,1) == 3.f );
    REQUIRE( mat(1,2) == 0.f );
    REQUIRE( mat(2,0) == 0.f );
    REQUIRE( mat(2,1) == 0.f );
    REQUIRE( mat(2,2) == 3.f );
}

TEST_CASE("operator / scalar", "") {
    Mat3 mat;

    mat = mat / 2;

    REQUIRE( mat(0,0) == 0.5f );
    REQUIRE( mat(0,1) == 0.f );
    REQUIRE( mat(0,2) == 0.f );
    REQUIRE( mat(1,0) == 0.f );
    REQUIRE( mat(1,1) == 0.5f );
    REQUIRE( mat(1,2) == 0.f );
    REQUIRE( mat(2,0) == 0.f );
    REQUIRE( mat(2,1) == 0.f );
    REQUIRE( mat(2,2) == 0.5f );
}

TEST_CASE("operator *= scalar", "") {
    Mat3 mat;

    mat *= 3;

    REQUIRE( mat(0,0) == 3.f );
    REQUIRE( mat(0,1) == 0.f );
    REQUIRE( mat(0,2) == 0.f );
    REQUIRE( mat(1,0) == 0.f );
    REQUIRE( mat(1,1) == 3.f );
    REQUIRE( mat(1,2) == 0.f );
    REQUIRE( mat(2,0) == 0.f );
    REQUIRE( mat(2,1) == 0.f );
    REQUIRE( mat(2,2) == 3.f );
}

TEST_CASE("operator /= scalar", "") {
    Mat3 mat;

    mat /= 2;

    REQUIRE( mat(0,0) == 0.5f );
    REQUIRE( mat(0,1) == 0.f );
    REQUIRE( mat(0,2) == 0.f );
    REQUIRE( mat(1,0) == 0.f );
    REQUIRE( mat(1,1) == 0.5f );
    REQUIRE( mat(1,2) == 0.f );
    REQUIRE( mat(2,0) == 0.f );
    REQUIRE( mat(2,1) == 0.f );
    REQUIRE( mat(2,2) == 0.5f );
}

TEST_CASE("operator + Mat3", "") {
    Mat3 matA;
    Mat3 matB;

    Mat3 mat = matA + matB;

    REQUIRE( mat(0,0) == 2.f );
    REQUIRE( mat(0,1) == 0.f );
    REQUIRE( mat(0,2) == 0.f );
    REQUIRE( mat(1,0) == 0.f );
    REQUIRE( mat(1,1) == 2.f );
    REQUIRE( mat(1,2) == 0.f );
    REQUIRE( mat(2,0) == 0.f );
    REQUIRE( mat(2,1) == 0.f );
    REQUIRE( mat(2,2) == 2.f );
}

TEST_CASE("operator - Mat3", "") {
    Mat3 matA;
    Mat3 matB;

    Mat3 mat = matA - matB;

    REQUIRE( mat(0,0) == 0.f );
    REQUIRE( mat(0,1) == 0.f );
    REQUIRE( mat(0,2) == 0.f );
    REQUIRE( mat(1,0) == 0.f );
    REQUIRE( mat(1,1) == 0.f );
    REQUIRE( mat(1,2) == 0.f );
    REQUIRE( mat(2,0) == 0.f );
    REQUIRE( mat(2,1) == 0.f );
    REQUIRE( mat(2,2) == 0.f );
}

TEST_CASE("operator * Mat3", "") {
    Mat3 matA (1, 0, 1, 0, 1, 0, 1, 0, 1);
    Mat3 matB (1, 0, 1, 0, 1, 0, 1, 0, 1);

    Mat3 mat = matA * matB;

    REQUIRE( mat(0,0) == 2.f );
    REQUIRE( mat(0,1) == 0.f );
    REQUIRE( mat(0,2) == 2.f );
    REQUIRE( mat(1,0) == 0.f );
    REQUIRE( mat(1,1) == 1.f );
    REQUIRE( mat(1,2) == 0.f );
    REQUIRE( mat(2,0) == 2.f );
    REQUIRE( mat(2,1) == 0.f );
    REQUIRE( mat(2,2) == 2.f );
}

TEST_CASE("operator += Mat3", "") {
    Mat3 mat;
    Mat3 matB;

    mat += matB;

    REQUIRE( mat(0,0) == 2.f );
    REQUIRE( mat(0,1) == 0.f );
    REQUIRE( mat(0,2) == 0.f );
    REQUIRE( mat(1,0) == 0.f );
    REQUIRE( mat(1,1) == 2.f );
    REQUIRE( mat(1,2) == 0.f );
    REQUIRE( mat(2,0) == 0.f );
    REQUIRE( mat(2,1) == 0.f );
    REQUIRE( mat(2,2) == 2.f );
}

TEST_CASE("operator -= Mat3", "") {
    Mat3 mat;
    Mat3 matB;

    mat -= matB;

    REQUIRE( mat(0,0) == 0.f );
    REQUIRE( mat(0,1) == 0.f );
    REQUIRE( mat(0,2) == 0.f );
    REQUIRE( mat(1,0) == 0.f );
    REQUIRE( mat(1,1) == 0.f );
    REQUIRE( mat(1,2) == 0.f );
    REQUIRE( mat(2,0) == 0.f );
    REQUIRE( mat(2,1) == 0.f );
    REQUIRE( mat(2,2) == 0.f );
}

TEST_CASE("operator *= Mat3", "") {
    Mat3 mat (1, 0, 1, 0, 1, 0, 1, 0, 1);
    Mat3 matB (1, 0, 1, 0, 1, 0, 1, 0, 1);

    mat *= matB;

    REQUIRE( mat(0,0) == 2.f );
    REQUIRE( mat(0,1) == 0.f );
    REQUIRE( mat(0,2) == 2.f );
    REQUIRE( mat(1,0) == 0.f );
    REQUIRE( mat(1,1) == 1.f );
    REQUIRE( mat(1,2) == 0.f );
    REQUIRE( mat(2,0) == 2.f );
    REQUIRE( mat(2,1) == 0.f );
    REQUIRE( mat(2,2) == 2.f );
}

TEST_CASE("operator == Mat3", "") {
    Mat3 matA;
    Mat3 matB (1, 0, 0, 0, 1, 0, 0, 0, 1);

    bool pass = false;

    if ((matA[0] == matB[0]) &&(matA[1] == matB[1]) && (matA[2] == matB[2])) {
        pass = true;
    }

    REQUIRE( pass );
}


TEST_CASE("operator != Mat3", "") {
    Mat3 matA;
    Mat3 matB (1, 1, 0, 1, 1, 0, 0, 1, 1);

    bool pass = false;

    if ((matA[0] != matB[0]) || (matA[1] != matB[1]) || (matA[2] != matB[2])) {
        pass = true;
    }

    REQUIRE( pass );
}

TEST_CASE("transpose", "") {
    Mat3 mat (1, 2, 3, 4, 5, 6, 7, 8, 9);
    mat = Mat3::transpose(mat);

    REQUIRE( mat(0,0) == 1.f );
    REQUIRE( mat(0,1) == 2.f );
    REQUIRE( mat(0,2) == 3.f );
    REQUIRE( mat(1,0) == 4.f );
    REQUIRE( mat(1,1) == 5.f );
    REQUIRE( mat(1,2) == 6.f );
    REQUIRE( mat(2,0) == 7.f );
    REQUIRE( mat(2,1) == 8.f );
    REQUIRE( mat(2,2) == 9.f );
}

TEST_CASE("det", "") {
    Mat3 mat (3, -3, 1, 2, 1, 0, 0, 0, 1);
    float det = Mat3::det(mat);

    REQUIRE( det == 9.f );
}

// TEST_CASE("cof", "") {
//     Mat3 mat (1, 2, 3, 4, 5, 6, 7, 8, 9);
//     mat = Mat3::cof(mat);

//     REQUIRE( mat(0,0) == -3.f );
//     REQUIRE( mat(0,1) == 6.f );
//     REQUIRE( mat(0,2) == -3.f );
//     REQUIRE( mat(1,0) == 6.f );
//     REQUIRE( mat(1,1) == -12.f );
//     REQUIRE( mat(1,2) == 6.f );
//     REQUIRE( mat(2,0) == -3.f );
//     REQUIRE( mat(2,1) == 6.f );
//     REQUIRE( mat(2,2) == -3.f );
// }

// TEST_CASE("inv", "") {
//     Mat3 mat (1, 0, 1, 1, 3, 5, 1, 0, 2);
//     mat = Mat3::inv(mat);

//     REQUIRE( mat(0,0) == 2.f );
//     REQUIRE( mat(0,1) == 0.f );
//     REQUIRE( mat(0,2) == -1.f );
//     REQUIRE( mat(1,0) == 1.f );
//     REQUIRE( mat(1,1) == 1/3 );
//     REQUIRE( mat(1,2) == -4/3 );
//     REQUIRE( mat(2,0) == -1.f );
//     REQUIRE( mat(2,1) == 0.f );
//     REQUIRE( mat(2,2) == 1.f );
// }

