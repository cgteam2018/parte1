#include "Mat3.hpp"
#include <iostream>

Mat3::Mat3() {
    c0 = Vec3(1.0F, 0.0F, 0.0F);
    c1 = Vec3(0.0F, 1.0F, 0.0F);
    c2 = Vec3(0.0F, 0.0F, 1.0F);
}

Mat3::Mat3(const Vec3& vecA, const Vec3& vecB, const Vec3& vecC){
    c0 = vecA;
    c1 = vecB;
    c2 = vecC;
}

Mat3::Mat3(float a00, float a01, float a02, 
           float a10, float a11, float a12,
           float a20, float a21, float a22) {

    c0 = Vec3(a00, a10, a20);
    c1 = Vec3(a01, a11, a21);
    c2 = Vec3(a02, a12, a22);
}

Vec3 Mat3::operator [] (int col) const {
    switch(col) {
        case 0 : return c0;
        case 1 : return c1;
        case 2 : return c2;
    }
}

Vec3& Mat3::operator [] (int col) {
    switch(col) {
        case 0 : return this->c0;
        case 1 : return this->c1;
        case 2 : return this->c2;
    }
}

float Mat3::operator () (int col, int row) const{
    switch(col) {
        case 0 :
            switch(row) {
                case 0 : return c0[0];  
                case 1 : return c0[1];
                case 2 : return c0[2];
            };
        case 1 :
            switch(row) {
                case 0 : return c1[0];  
                case 1 : return c1[1];
                case 2 : return c1[2];
            };
        case 2 :
            switch(row) {
                case 0 : return c2[0];  
                case 1 : return c2[1];
                case 2 : return c2[2];
        };
    }
}

float& Mat3::operator () (int col, int row){
    switch(col) {
        case 0 :
            switch(row) {
                case 0 : return this->c0[0];  
                case 1 : return this->c0[1];
                case 2 : return this->c0[2];
            };
        case 1 :
            switch(row) {
                case 0 : return this->c1[0];  
                case 1 : return this->c1[1];
                case 2 : return this->c1[2];
            };
        case 2 :
            switch(row) {
                case 0 : return this->c2[0];  
                case 1 : return this->c2[1];
                case 2 : return this->c2[2];
        };
    }
}

const Mat3 Mat3::operator * (float scalar) const{
    Mat3 result (c0 * scalar, c1 * scalar, c2 * scalar);
    return result;
}

const Mat3 Mat3::operator / (float scalar) const{
    Mat3 result (c0 / scalar, c1 / scalar, c2 / scalar);
    return result;
}

const void Mat3::operator *= (float scalar) {
    c0 *= scalar;
    c1 *= scalar;
    c2 *= scalar;
}

const void Mat3::operator /= (float scalar) {
    c0 /= scalar;
    c1 /= scalar;
    c2 /= scalar;
}

const Mat3 Mat3::operator + (const Mat3& mat) const {
    Mat3 result (c0 + mat[0], c1 + mat[1], c2 + mat[2]);
    return result;
}

const Mat3 Mat3::operator - (const Mat3& mat) const {
    Mat3 result (c0 - mat[0], c1 - mat[1], c2 - mat[2]);
    return result;
}

const Mat3 Mat3::operator * (const Mat3& mat) const {
    Mat3 C;

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            float sum = 0.0;
            for (int k = 0; k < 3; k++) {
                sum += (this->operator()(k, i) * mat(j, i));
            }
            C(j, i) = sum;
        }
    }

    return C;
}

const void Mat3::operator += (const Mat3& mat) {
    c0 += mat[0];
    c1 += mat[1];
    c2 += mat[2];
}

const void Mat3::operator -= (const Mat3& mat) {
    c0 -= mat[0];
    c1 -= mat[1];
    c2 -= mat[2];
}

const void Mat3::operator *= (const Mat3& mat) {
    Mat3 C;

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            float sum = 0.0;
            for (int k = 0; k < 3; k++) {
                sum += (this->operator()(k, i) * mat(j, i));
            }
            C(j, i) = sum;
        }
    }

    c0 = C[0];
    c1 = C[1];
    c2 = C[2];
}

const bool Mat3::operator == (const Mat3& mat) const {
    if ((c0 == mat[0]) && (c1 == mat[1]) && (c2 == mat[2])) {
        return true;
    } else {
        return false;
    }
}

const bool Mat3::operator != (const Mat3& mat) const {
    if ((c0 != mat[0]) && (c1 != mat[1]) && (c2 != mat[2])) {
        return true;
    } else {
        return false;
    }
}

const Mat3 Mat3::transpose(const Mat3& mat) {
    Mat3 result;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            result(j, i) = mat(i, j);
        }
    }

    return result;
}

const float Mat3::det(const Mat3& mat) {
    float det = (mat(0, 0)*mat(1, 1)*mat(2,2) + mat(0,1)*mat(1,2)*mat(2,0) + mat(0,2)*mat(1,0)*mat(2,1)) - 
                (mat(0, 0)*mat(1, 2)*mat(2,1) + mat(0,1)*mat(1,0)*mat(2,2) + mat(0,2)*mat(1,1)*mat(2,0));
    
    return det;
}

// const Mat3 cof(const Mat3& mat) {

// }

const Mat3 Mat3::inv(const Mat3& mat) {
    // falta implementar
}