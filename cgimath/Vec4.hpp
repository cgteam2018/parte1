// #include "Mat4.h"

class Vec4 {
    private:
        float x, y, z, w;  // coordenadas do vetor
    
    public:
        // construtores
        Vec4();
        Vec4(float i, float j, float k, float l);   // x = i, y = j, z = k, w = l

        // sobrecarga de operadores

        // acesso via índices
        float operator [] (int i) const;
        float& operator [] (int i);

        // vetor com escalar
        const Vec4 operator * (float scalar) const;
        const Vec4 operator / (float scalar) const;
        const void operator *= (float scalar);
        const void operator /= (float scalar);

        //vetor com vetor
        const Vec4 operator + (const Vec4& vec) const;
        const Vec4 operator - (const Vec4& vec) const;
        const bool operator == (const Vec4& vec) const;
        const bool operator != (const Vec4& vec) const;
        const void operator += (const Vec4& vec);
        const void operator -= (const Vec4& vec);

        // matriz com vetor
        // const void operator * (const Mat3& mat); // supostamente aplica uma matriz ao vetor, não sei se é pra implementa

        // vetores apontando para cada direção
        static const Vec4 UP();
        static const Vec4 DOWN();
        static const Vec4 LEFT();
        static const Vec4 RIGHT();
        static const Vec4 BACK();
        static const Vec4 FOWARD();

        // vetores (1, 1, 1, 1) e (0, 0, 0, 0)
        static const Vec4 ONE();
        static const Vec4 ZERO();

        // operacoes entre vetores
        static float dot_product(const Vec4& vecA,const Vec4& vecB);
        static float magnitude(const Vec4& vec);
		static const Vec4 normalized(const Vec4& vec);

        // representação visual
        void to_string();
};