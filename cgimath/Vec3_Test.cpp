#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "Vec3.hpp"
#include <iostream>

TEST_CASE("Vec3()", "") {
    Vec3 vec;
    REQUIRE( vec[0] == 0.f );
    REQUIRE( vec[1] == 0.f );
    REQUIRE( vec[2] == 0.f );
}

TEST_CASE("Vec3(5.78, 0, -98.3)", "") {
    Vec3 vec (5.78, 0, -98.3);
    REQUIRE( vec[0] == 5.78f );
    REQUIRE( vec[1] == 0.00f );
    REQUIRE( vec[2] == -98.30f );
}

TEST_CASE("operator []", "") {
    Vec3 vec (5.78, 0, -98.3);
    vec[0] = -38.97;
    vec[1] = 987.29;
    vec[2] = 0;
    REQUIRE( vec[0] == -38.97f );
    REQUIRE( vec[1] == 987.29f );
    REQUIRE( vec[2] == 0.0f );
}

TEST_CASE("operator * scalar", "") {
    Vec3 vec (1, 0, -1);
    Vec3 vecr = vec * 3;
    REQUIRE( vecr[0] == 3.0f );
    REQUIRE( vecr[1] == 0.0f );
    REQUIRE( vecr[2] == -3.0f );
}

TEST_CASE("operator / scalar", "") {
    Vec3 vec (3, 0, -3);
    Vec3 vecr = vec / 3;
    REQUIRE( vecr[0] == 1.0f );
    REQUIRE( vecr[1] == 0.0f );
    REQUIRE( vecr[2] == -1.0f );
}

TEST_CASE("operator *= scalar", "") {
    Vec3 vec (1, 0, -1);
    vec *= 3;
    REQUIRE( vec[0] == 3.0f );
    REQUIRE( vec[1] == 0.0f );
    REQUIRE( vec[2] == -3.0f );
}

TEST_CASE("operator /= scalar", "") {
    Vec3 vec (3, 0, -3);
    vec /= 3;
    REQUIRE( vec[0] == 1.0f );
    REQUIRE( vec[1] == 0.0f );
    REQUIRE( vec[2] == -1.0f );
}

TEST_CASE("operator + Vec3", "") {
    Vec3 vecA (3, 2, -3);
    Vec3 vecB (-2, -2, 2);
    Vec3 vec = vecA + vecB;
    REQUIRE( vec[0] == 1.0f );
    REQUIRE( vec[1] == 0.0f );
    REQUIRE( vec[2] == -1.0f );
}

TEST_CASE("operator - Vec3", "") {
    Vec3 vecA (3, 2, -3);
    Vec3 vecB (-2, -2, 2);
    Vec3 vec = vecA - vecB;
    REQUIRE( vec[0] == 5.0f );
    REQUIRE( vec[1] == 4.0f );
    REQUIRE( vec[2] == -5.0f );
}

TEST_CASE("operator += Vec3", "") {
    Vec3 vecA (3, 2, -3);
    Vec3 vecB (-2, -2, 2);
    vecA += vecB;
    REQUIRE( vecA[0] == 1.0f );
    REQUIRE( vecA[1] == 0.0f );
    REQUIRE( vecA[2] == -1.0f );
}

TEST_CASE("operator -= Vec3", "") {
    Vec3 vecA (3, 2, -3);
    Vec3 vecB (-2, -2, 2);
    vecA -= vecB;
    REQUIRE( vecA[0] == 5.0f );
    REQUIRE( vecA[1] == 4.0f );
    REQUIRE( vecA[2] == -5.0f );
}

TEST_CASE("operator == Vec3", "") {
    Vec3 vecA (3.25, 2.31, -3.44);
    Vec3 vecB (3, 2, -4);
    bool pass = false;
    if (vecA == vecB) {
        pass = true;
    }
    REQUIRE( pass );
}

TEST_CASE("operator != Vec3", "") {
    Vec3 vecA (3.25, 2.31, -3.44);
    Vec3 vecB (3, 2, -2);
    bool pass = false;
    if (vecA != vecB) {
        pass = true;
    }
    REQUIRE( pass );
}

TEST_CASE("UP", "") {
    Vec3 vec = Vec3::UP();
    REQUIRE ( vec[0] == 0.f );
    REQUIRE ( vec[1] == 1.f );
    REQUIRE ( vec[2] == 0.f );
}

TEST_CASE("DOWN", "") {
    Vec3 vec = Vec3::DOWN();
    REQUIRE ( vec[0] == 0.f );
    REQUIRE ( vec[1] == -1.f );
    REQUIRE ( vec[2] == 0.f );
}

TEST_CASE("LEFT", "") {
    Vec3 vec = Vec3::LEFT();
    REQUIRE ( vec[0] == -1.f );
    REQUIRE ( vec[1] == 0.f );
    REQUIRE ( vec[2] == 0.f );
}

TEST_CASE("RIGHT", "") {
    Vec3 vec = Vec3::RIGHT();
    REQUIRE ( vec[0] == 1.f );
    REQUIRE ( vec[1] == 0.f );
    REQUIRE ( vec[2] == 0.f );
}

TEST_CASE("BACK", "") {
    Vec3 vec = Vec3::BACK();
    REQUIRE ( vec[0] == 0.f );
    REQUIRE ( vec[1] == 0.f );
    REQUIRE ( vec[2] == -1.f );
}

TEST_CASE("FOWARD", "") {
    Vec3 vec = Vec3::FOWARD();
    REQUIRE ( vec[0] == 0.f );
    REQUIRE ( vec[1] == 0.f );
    REQUIRE ( vec[2] == 1.f );
}

TEST_CASE("ONE", "") {
    Vec3 vec = Vec3::ONE();
    REQUIRE ( vec[0] == 1.f );
    REQUIRE ( vec[1] == 1.f );
    REQUIRE ( vec[2] == 1.f );
}

TEST_CASE("ZERO", "") {
    Vec3 vec = Vec3::ZERO();
    REQUIRE ( vec[0] == 0.f );
    REQUIRE ( vec[1] == 0.f );
    REQUIRE ( vec[2] == 0.f );
}

TEST_CASE("dot product", "") {
    Vec3 vecA (0, -3, 2);
    Vec3 vecB (1, 2, -3);
    float dot_prod = Vec3::dot_product(vecA, vecB);
    REQUIRE ( dot_prod == -12.f );
}

TEST_CASE("cross product", "") {
    Vec3 vecA (3, 2, 1);
    Vec3 vecB (1, 2, 3);
    Vec3 vec = Vec3::cross_product(vecA, vecB);
    REQUIRE ( vec[0] == 4.f );
    REQUIRE ( vec[1] == -8.f );
    REQUIRE ( vec[2] == 4.f );
}

TEST_CASE("magnitude", "") {
    Vec3 vecA (0, -3, 4);
    float mag = Vec3::magnitude(vecA);
    REQUIRE ( mag == 5.f );
}

TEST_CASE("normalized", "") {
    Vec3 vecA (0, -3, 4);
    Vec3 norm = Vec3::normalized(vecA);
    REQUIRE ( norm[0] == 0.f );
    REQUIRE ( norm[1] == -3.f/5.f );
    REQUIRE ( norm[2] == 4.f/5.f );
}

TEST_CASE("angle", "") {
    Vec3 vecA (0, 1, 0);
    Vec3 vecB (1, 0, 0);
    float angle = Vec3::angle(vecA, vecB);

    REQUIRE ( angle == 90.f );
}

TEST_CASE("spherical", "") {
    Vec3 vecA (0, 1, 1);
    Vec3 vec = Vec3::spherical(vecA);

    //REQUIRE ( vec[0] == 1.41421f );
    REQUIRE ( vec[1] == 90.f );
    REQUIRE ( vec[2] == 45.f );
}

TEST_CASE("cartesian", "") {
    Vec3 vecA (1.4142135, 90, 45);
    Vec3 vec = Vec3::cartesian(vecA);
    //REQUIRE ( vec[0] == -0.f );
    REQUIRE ( ceil(vec[1]) == 1.f );
    REQUIRE ( ceil(vec[2]) == 1.f );
}