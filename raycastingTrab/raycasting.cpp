#include <iostream>
#include <cmath>

// #include "Vec3.hpp"
// #include "Point.hpp"
// #include "Material.hpp"
#include "Sphere.hpp"
#include "Light.hpp"

#ifdef __APPLE__
#include <GLUT/glut.h>
#include <OpenGL/glext.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/glext.h>
#pragma comment(lib, "glew32.lib")
#endif

#define PI 3.14159265

#define WINDOW_HEIGHT 500
#define WINDOW_WIDTH 500
#define WINDOW_POSITION -10

#define CAMERA_X 250
#define CAMERA_Y 250
#define CAMERA_Z -500

#define I_ENV_X 0.9
#define I_ENV_Y 1
#define I_ENV_Z 1

using namespace std;

Point camera(CAMERA_X, CAMERA_Y, CAMERA_Z);

Point light_origin(20, 10, -400);
Vec3 light_color(1, 1, 1);
//Vec3 light_color(0.70, 0.20, 0.40);
Light light(light_origin, light_color);

Vec3 skin_env(0.8, 0.8, 0.8);
Vec3 skin_dif(0.6, 0.6, 0.6);
Vec3 skin_spe(0.8, 0.8, 0.8);
Material skin(skin_env, skin_dif, skin_spe);

Vec3 eye_env(0, 0, 0);
Vec3 eye_dif(1, 1, 1);
Vec3 eye_spe(1.0, 1.0, 1.0);
Material eyec(eye_env, eye_dif, eye_spe);

Vec3 nose_env(0.8, 0.4, 0.0);
Vec3 nose_dif(1, 1, 1);
Vec3 nose_spe(1.0, 1.0, 1.0);
Material nosec(nose_env, nose_dif, nose_spe);

Vec3 button_env(0.1, 0.0, 0.0);
Vec3 button_dif(1, 1, 1);
Vec3 button_spe(1.0, 1.0, 1.0);
Material buttonc(button_env, button_dif, button_spe);

Point head_origin(250, 270, -400);
Sphere head(head_origin, 75, skin);

Point body_origin(250, 235, -400);
Sphere body(body_origin, 300, skin);

Point eye1_origin(245, 275, -400);
Sphere eye1(eye1_origin, 3, eyec);

Point eye2_origin(255, 275, -400);
Sphere eye2(eye2_origin, 3, eyec);

Point nose_origin(250, 268, -400);
Sphere nose(nose_origin, 2, nosec);

Point button1_origin(250, 235, -400);
Sphere button1(button1_origin, 2, buttonc);

Point button2_origin(250, 220, -400);
Sphere button2(button2_origin, 2, buttonc);

Point button3_origin(250, 250, -400);
Sphere button3(button3_origin, 2, buttonc);

Sphere spheres[] = { eye1, eye2, nose, button1, button2, button3, head, body };

// Drawing routine.
void drawScene(void) {
    glClear(GL_COLOR_BUFFER_BIT);

    glColor3f(0.0, 0.0, 0.0);

    glEnable(GL_PROGRAM_POINT_SIZE_EXT);
    glPointSize(1);

    glBegin(GL_POINTS);
        for (float i = 0; i < WINDOW_HEIGHT; i++) {
            for (float j = 0; j < WINDOW_WIDTH; j++) {
                Point pixel(i, j, WINDOW_POSITION);

                for (Sphere s : spheres) {
                    //Vec3 camera_pixel(camera.x - pixel.x, camera.y - pixel.y, camera.z - pixel.z);
                    Vec3 camera_pixel(pixel.x - camera.x, pixel.y - camera.y, pixel.z - camera.z);
                    float alpha = Vec3::dot_product(camera_pixel, camera_pixel);

                    Vec3 camera_scenter(camera.x - s.center.x, camera.y - s.center.y, camera.z - s.center.z);
                    //Vec3 camera_scenter(s.center.x - camera.x, s.center.y - camera.y, s.center.z - camera.z);
                    float beta = 2 * Vec3::dot_product(camera_pixel, camera_scenter);

                    Vec3 camera_camera(camera.x, camera.y, camera.z);
                    Vec3 center_center(s.center.x, s.center.y, s.center.z);
                    float gamma = Vec3::dot_product(camera_camera, camera_camera)
                                - 2 * Vec3::dot_product(camera_camera, center_center)
                                + Vec3::dot_product(center_center, center_center)
                                - 2 * s.radius;

                    float delta = beta * beta - 4 * alpha * gamma;

                    Vec3 I_ENV(I_ENV_X, I_ENV_Y, I_ENV_Z);
                    if (delta >= 0) {
                        float stretch = min((-beta + sqrt(delta))/(2*alpha), (-beta - sqrt(delta))/(2*alpha));
                        camera_pixel *= stretch;

                        Point intercepted_point(camera);
                        intercepted_point += camera_pixel;

                        Vec3 N(s.center.x - intercepted_point.x, s.center.y - intercepted_point.y, s.center.z - intercepted_point.z);
                        //Vec3 N(intercepted_point.x - s.center.x, s.center.y - intercepted_point.y, intercepted_point.z - intercepted_point.z);
                        Vec3 n = Vec3::normalized(N);

                        Vec3 L(intercepted_point.x - light.origin.x, intercepted_point.y - light.origin.y, intercepted_point.z - light.origin.z);
                        //Vec3 L(light.origin.x - intercepted_point.x, light.origin.y - intercepted_point.y, light.origin.z - intercepted_point.z);
                        Vec3 l = Vec3::normalized(L);

                        Vec3 k_env(s.material.environment);
                        Vec3 k_dif(s.material.diffuse);
                        Vec3 k_spe(s.material.specular);

                        Vec3 light_rate(light.color);

                        Vec3 i_dif(light_rate);
                        i_dif.at(k_dif);
                        float f_dif = Vec3::dot_product(l, n);
                        i_dif *= f_dif;

                        Vec3 i_spe(light_rate);
                        i_spe.at(k_spe);
                        Vec3 r(n);
                        r *= (2 * Vec3::dot_product(l, n));
                        r -= l;
                        Vec3 intercepted_point_camera(camera.x - intercepted_point.x, camera.y - intercepted_point.y, camera.z - intercepted_point.x);
                        //Vec3 intercepted_point_camera(intercepted_point.x - camera.x, intercepted_point.y - camera.y, intercepted_point.z - camera.z);
                        Vec3 v = Vec3::normalized(intercepted_point_camera);
                        float f_spe = Vec3::dot_product(r, v);
                        i_spe *= f_spe;

                        Vec3 color(I_ENV);
                        color.at(k_env);
                        color += i_dif;
                        color += i_spe;

                        glColor3f(color[0], color[1], color[2]);
                        break;
                    }
                    else {
                        glColor3f(I_ENV_X, I_ENV_Y, I_ENV_Z);
                    }
                }

                glVertex3f(i, j, 0);
            }
        }

    glEnd();
    glFlush();
}

// Initialization routine.
void setup(void) {
    glClearColor(1.0, 1.0, 1.0, 0.0);
}

// OpenGL window reshape routine.
void resize(int w, int h) {
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, WINDOW_WIDTH, 0.0, WINDOW_HEIGHT, -500.0, 0.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y) {
    switch(key) {
        case 27:
            exit(0);
            break;
        default:
            break;
    }
}

// Main routine.
int main(int argc, char **argv) {
    glutInit(&argc, argv);

    #ifdef _WIN32
    glutInitContextVersion(4, 3);
    glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
    #endif

    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
    glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("RAYCASTING");
    glutDisplayFunc(drawScene);
    glutReshapeFunc(resize);
    glutKeyboardFunc(keyInput);

    #ifdef _WIN32
    glewExperimental = GL_TRUE;
    glewInit();
    #endif

    setup();

    glutMainLoop();
}