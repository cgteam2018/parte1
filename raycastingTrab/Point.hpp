#ifndef POINT_H
#define POINT_H

#pragma once

#include "Vec3.hpp"

class Point {
    public:
        float x, y, z;
        Point();
        Point(float rx, float ry, float rz);
        const void operator += (const Vec3 vec);
};

#endif