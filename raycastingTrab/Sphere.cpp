#include <iostream>
#include "Sphere.hpp"

Sphere::Sphere(Point c, float r, Material m) {
    center = c;
    radius = r;
    material = m;
}