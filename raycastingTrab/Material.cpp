#include <iostream>
#include "Material.hpp"

Material::Material() {
    
}

Material::Material(Vec3 vecA, Vec3 vecB, Vec3 vecC) {
    environment = vecA;
    diffuse = vecB;
    specular = vecC;
}