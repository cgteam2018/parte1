#ifndef SPHERE_H
#define SPHERE_H

#include "Point.hpp"
#include "Material.hpp"

class Sphere {
    public:
        Point center;
        float radius;
        Material material;
        Sphere();
        Sphere(Point c, float r, Material m);
};

#endif