#include <iostream>
#include "Point.hpp"

Point::Point() {
    
}

Point::Point(float rx, float ry, float rz) {
    x = rx;
    y = ry;
    z = rz;
}

const void Point::operator += (const Vec3 vec) {
    x += vec[0];
    y += vec[1];
    z += vec[2];
}