#ifndef LIGHT_H
#define LIGHT_H

#include "Vec3.hpp"
#include "Point.hpp"

class Light {
    public:
        Point origin;
        Vec3 color;
        Light();
        Light(Point o, Vec3 c);
};

#endif