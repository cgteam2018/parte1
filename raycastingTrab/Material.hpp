#ifndef MATERIAL_H
#define MATERIAL_H

#include "Vec3.hpp"

class Material {
    public:
        Vec3 environment;
        Vec3 diffuse;
        Vec3 specular;
        Material();
        Material(Vec3 vecA, Vec3 vecB, Vec3 vecC);
};

#endif