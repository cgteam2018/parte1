#ifndef VECTOR3_H
#define VECTOR3_H


class Vector3 {
    private:
        float x, y, z;  // coordenadas do vetor

    public:
        // construtores
        Vector3(); // inicia com (0, 0, 0)
        Vector3(float i, float j, float k);    // inicia x = i, y = j, z = k

        // sobrecarga de operadores

        // acesso via �ndices
        float operator [] (int i) const;
        float& operator [] (int i);

        // vetor com escalar
        const Vector3 operator * (float scalar) const;
        const Vector3 operator / (float scalar) const;
        const void operator *= (float scalar);
        const void operator /= (float scalar);

        //vetor com vetor
        const Vector3 operator + (const Vector3& vec) const;
        const Vector3 operator - (const Vector3& vec) const;
        const bool operator == (const Vector3& vec) const;
        const bool operator != (const Vector3& vec) const;
        const void operator += (const Vector3& vec);
        const void operator -= (const Vector3& vec);

        // matriz com vetor
        // const void operator * (const Mat3& mat); // supostamente aplica uma matriz ao vetor, n�o sei se � pra implementar

        // transforma��es de coordenadas cartesianas para esf�ricas e vice-versa
        static const Vector3 spherical(const Vector3& vec);
		static const Vector3 cartesian(const Vector3& vec);

        // vetores apontando para cada dire��o
        static const Vector3 UP();
        static const Vector3 DOWN();
        static const Vector3 LEFT();
        static const Vector3 RIGHT();
        static const Vector3 BACK();
        static const Vector3 FOWARD();

        // vetores (1, 1, 1) e (0, 0, 0)
        static const Vector3 ONE();
        static const Vector3 ZERO();

        // operacoes entre vetores
        static float dot_product(const Vector3& vecA,const Vector3& vecB);      // produto escalar
        static const Vector3 cross_product(const Vector3& vecA,const Vector3& vecB);   // produto vetorial
        static float magnitude(const Vector3& vec);     // tamanho
		static const Vector3 normalized(const Vector3& vec);   // norma
        static const float angle(const Vector3& vecA, const Vector3& vecB);

        // representa��o visual
        void to_string();
};
#endif // VECTOR3_H
