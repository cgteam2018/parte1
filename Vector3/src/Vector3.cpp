#include "Vector3.h"
#include <iostream>

Vector3::Vector3() {
    x = 0;
    y = 0;
    z = 0;
}

Vector3::Vector3(float i, float j, float k) {
    x = i;
    y = j;
    z = k;
}

/*Metodos de acesso*/
float Vector3::operator [] (int position) const {
    switch(position) {
        case 0 : return x;
        case 1 : return y;
        case 2 : return z;
    }
}

float& Vector3::operator [] (int position) {
    switch(position) {
        case 0 : return this->x;
        case 1 : return this->y;
        case 2 : return this->z;
    }
}
/*Fim dos m�todos de acesso*/

/*Opera��es com escalar*/

const Vector3 Vector3::operator * (float scalar) const{
    int xr = x*scalar;
    int yr = y*scalar;
    int zr = z*scalar;

    Vector3 result(xr, yr, zr);
    return result;
}
const Vector3 Vector3::operator / (float scalar) const{
    int xr = x/scalar;
    int yr = y/scalar;
    int zr = z/scalar;

    Vector3 result(xr, yr, zr);
    return result;
}
const void Vector3::operator *= (float scalar){
    x=x*scalar;
    y=y*scalar;
    z=z*scalar;

}
const void Vector3::operator /= (float scalar){
    x=x/scalar;
    y=y/scalar;
    z=z/scalar;

}
/*Fim das opera��es com escalar*/

/*Opera��es com vetor*/
const Vector3 Vector3::operator + (const Vector3& vec) const{
    int xr = x + vec[0];
    int yr = y + vec[1];
    int zr = z + vec[2];

    Vector3 result(xr, yr, zr);
    return result;
}
const Vector3 Vector3::operator - (const Vector3& vec) const{
    int xr = x - vec[0];
    int yr = y - vec[1];
    int zr = z - vec[2];

    Vector3 result(xr, yr, zr);
    return result;
}
const bool Vector3::operator == (const Vector3& vec) const{

}
const bool Vector3::operator != (const Vector3& vec) const{

}
const void Vector3::operator += (const Vector3& vec){
    x = x + vec[0];
    y = y + vec[1];
    z = z + vec[2];
}
const void Vector3::operator -= (const Vector3& vec){
    x = x - vec[0];
    y = y - vec[1];
    z = z - vec[2];
}
/*Fim das opera��es com vetor*/

/* vetores apontando para cada dire��o*/
static const Vector3 Vector3::UP(){
}
static const Vector3 Vector3::DOWN(){
}
static const Vector Vector3::LEFT(){
}
static const Vector3 Vector3::RIGHT(){
}
static const Vector3 Vector3::BACK(){
}
static const Vector3 Vector3::FOWARD(){
}

/* vetores (1, 1, 1) e (0, 0, 0)*/
static const Vector3 Vector3::ONE(){
    static Vector3 vOne(1, 1, 1);
    return vOne;
}
static const Vector3 Vector3::ZERO(){
    static Vector3 vZero(0, 0, 0);
    return vZero;
}


/* operacoes entre vetores*/
// produto escalar
static float Vector3::dot_product(const Vector3& vecA,const Vector3& vecB){
    return vecA[0]*vecB[0] + vecA[1]*vecB[1] + vecA[2]*vecB[2];
}

// produto vetorial
static const Vector3 Vector3::cross_product(const Vector3& vecA,const Vector3& vecB){
}

// tamanho
static float Vector3::magnitude(const Vector3& vec){
}

// norma
static const Vector3 Vector3::normalized(const Vector3& vec){
}

//angulo entre dois vetores
static const float Vector3::angle(const Vector3& vecA, const Vector3& vecB){
}



void Vector3::to_string() {
    std::cout << "(" << x << ", " << y << ", " << z << ")" << "\n";
}
